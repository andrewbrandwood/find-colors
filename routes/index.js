
var WebsiteController = function (website) {
	// Public functions
	this.get = function(req, res) {
		var url = req.params.loc;
		if(url === '/'){
			url = 'default';
		}
		res.render('index', createModel({url: url}));
	};

	var createModel = function (params) {

		var model = {
			viewId: 'wesite-home',
			controller: 'website-home-controller',
			title: 'Find Colors',
			site: params.url
		}

		return model;

	}
};

module.exports = function(website) {
	var controller = new WebsiteController(website);

	website.get('/', controller.get);
};
