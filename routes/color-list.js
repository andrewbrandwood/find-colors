fs = require('fs');
var request = require('request');
var MongoClient = require('mongodb').MongoClient;
var moment = require('moment');

var WebsiteController = function (website) {

	var colArr = [];
	var remotePrettyUrl = '';
	var uri = website.settings.dbconnection;

	this.get = function(req, res){
		colArr = [];
		cssTable = {};

		var localMatch = /(?:f|ht)tps?:\/\//g;
		var url = req.query.remoteUrl;



		if(!localMatch.exec(url)){
			url = 'http://' + url;
		}

		remotePrettyUrl = req.query.remoteUrl;

		request({uri: url}, function(err, response, body){
	    var self = this;
			//Just a basic error check
  		if(err && res.statusCode !== 200){console.log('Request error.');}

			var remoteUrls = [];
			var htmlTag;
			var stylesheetTags = '';
			var obj = {  date: new Date(), leaderboardName: req.query['leaderboard-name'] || 'anon' }
			var linkRegex = /<\/?\w+((\s+\w+(\s*=\s*(?:".*?"|'.*?'|[\^'">\s]+))?)+\s*|\s*)\/?>/g;
			while (htmlTag = linkRegex.exec(body)){
				if(htmlTag[0].indexOf('link') !== -1 && htmlTag[0].indexOf('stylesheet') !== -1){

					var href = htmlTag[0].match(/(href=\")([^\"]*)(\")/g);
					if(href === null) {
						href = htmlTag[0].match(/(href=\')([^\']*)(\')/g);
					}
					href = href.toString();
					href = href.replace('href=','').replace(new RegExp('"', 'g'), '').replace(new RegExp('\'', 'g'), '');
					remoteUrls.push(href);
				}
			}

			var cssUrls = [];
			for(var i = 0; i < remoteUrls.length; i++){
				var cssUrl = remoteUrls[i];
				if(cssUrl.match(localMatch) !== null){
					cssUrls.push(cssUrl);
					continue;
				}

				var serverHost = /^\/\//igm;

				if(cssUrl.match(localMatch) === null){
					if(cssUrl.match(serverHost) !== null){
							cssUrl = response.request.uri.protocol + cssUrl;
							cssUrls.push(cssUrl);
							continue;
					}


					var prot = response.request.uri.protocol + '//' + response.request.uri.host;
					if(cssUrl.charAt(0) !== '/' || cssUrl.charAt(0) !== '.'){
						cssUrl = prot + '/' + cssUrl;
					}else{
						cssUrl = prot + cssUrl;
					}
					cssUrls.push(cssUrl);
					continue;
				}

			}

			getCSS(res, cssUrls, obj);

		});

	};

	function getCSS(res, cssUrls, obj){
		var cssFile = [];
		var promiseList = [];

		for(var i = 0; i < cssUrls.length; i++){
			promiseList.push(
				new Promise((resolve, reject) => {
					request({uri: cssUrls[i]}, (err, response, body) => {
						if(err && res.statusCode !== 200){reject('Request error.');}
						if(body.indexOf('<!doctype') !== -1){
							resolve('');
						}else{
							resolve([body, cssUrls[i]]);
						}
					});
				}).catch((error) => {
					console.log(error,'Promise error');
				})
			);
		}

		Promise.all(promiseList).then(css => {
			cssFile = cssFile.concat(css).join('');
			getColors(cssFile, res, obj);
		}).catch((error) => {
			console.log(error,'Promise error');
		});

	}

	function filterColor(reg, str){
		var item;
		while (item = reg.exec(str)){
			var color = item[0].replace(';','');
			if(colArr.indexOf(color) === -1){
				colArr.push(color);
			}
		}
	}

	function addToDatabase(obj){
		getHighestColors()
		.then((results) => {

			//filter to see if the item already exists
			var siteNames = results.map(function (s, index, array) {
			    return s.colorTotal;
			});

			if(siteNames.filter((s) => s === obj.colorTotal).length >= 1){
				return;
			}

			MongoClient.connect(uri, function(err, db) {
				var collection = 'colorfinder';
				if(err){
					console.error(err);
					return;
				}

				db.collection(collection).insertOne(obj);
			  db.close();

			});
		});
	}

	function getHighestColors(){
    return new Promise((resolve, reject) => {
      MongoClient.connect(uri, function(err, db) {
        var collection = 'colorfinder';
  			if(err){
  				console.error(err);
  				return;
  			}
        db.collection(collection).find().sort({colorTotal: -1}).toArray((err, results) => {
  					if(err){
  						console.log(err);
  						reject(err);
  					}

            db.close();
            resolve(results);
  			});
      });
    }).catch((error) => {
      console.log(error);
    });
  }

	function getColors(str, res, obj){
		// find all strings staring with # and ending in;
		var reHex = /#\w+;/g,
		reRgb = /rgb\w+;/g;

		filterColor(reHex, str);
		filterColor(reRgb, str);

		var rgbArr = colArr.map(hexToRgb);
		var sortedRgbArr = sortColors(rgbArr);
		var uniqueArr = removeDuplicates(sortedRgbArr);
		var finalArray = uniqueArr.map(rgbToHex);

		var dbUrl = remotePrettyUrl.replace('http://', '').replace('https://','').toLowerCase();
		obj.colorArray = finalArray;
		obj.site = dbUrl;
		obj.colorTotal = finalArray.length;
		//var obj = {colorArray: finalArray, site: dbUrl, colorTotal: finalArray.length, date: new Date(), leaderboardName: leaderboardName };
		if(finalArray.length > 0){
			addToDatabase(obj);
		}

		res.render('color-list', createModel(obj));

	}

	function removeDuplicates(a){
		var prims = {"boolean":{}, "number":{}, "string":{}}, objs = [];

    return a.filter(function(item) {
        var type = typeof item;
        if(type in prims)
            return prims[type].hasOwnProperty(item) ? false : (prims[type][item] = true);
        else
            return objs.indexOf(item) >= 0 ? false : objs.push(item);
    });
	}

	function hexToRgb(hex) {
			 // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
		var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
		hex = hex.replace(shorthandRegex, function(m, r, g, b) {
				return r + r + g + g + b + b;
		});

		var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
		return result ? [
				parseInt(result[1], 16), //r
				parseInt(result[2], 16), //g
				parseInt(result[3], 16) //b
		] : null;
	}

	function rgbToHex(rgb) {
			return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
	}

	function colorDistance(color1, color2) {
			// This is actually the square of the distance but
			// this doesn't matter for sorting.
			var result = 0;
			for (var i = 0; i < color1.length; i++)
					result += (color1[i] - color2[i]) * (color1[i] - color2[i]);
			return result;
	}

	function sortColors(colors) {

			if(colors.length <= 1) return colors;

			// Calculate distance between each color
			var distances = [];
			for (var i = 0; i < colors.length; i++) {
					distances[i] = [];
					for (var j = 0; j < i; j++){
						if(!colors[i]) continue;
						distances.push([colors[i], colors[j], colorDistance(colors[i], colors[j])]);
					}
			}
			distances.sort(function(a, b) {
					return a[2] - b[2];
			});

			// Put each color into separate cluster initially
			var colorToCluster = {};
			for (var i = 0; i < colors.length; i++)
					colorToCluster[colors[i]] = [colors[i]];

			// Merge clusters, starting with lowest distances
			var lastCluster;
			for (var i = 0; i < distances.length; i++) {
					var color1 = distances[i][0];
					var color2 = distances[i][1];
					var cluster1 = colorToCluster[color1];
					var cluster2 = colorToCluster[color2];
					if (!cluster1 || !cluster2 || cluster1 == cluster2)
							continue;

					// Make sure color1 is at the end of its cluster and
					// color2 at the beginning.
					if (color1 != cluster1[cluster1.length - 1])
							cluster1.reverse();
					if (color2 != cluster2[0])
							cluster2.reverse();

					// Merge cluster2 into cluster1
					cluster1.push.apply(cluster1, cluster2);
					delete colorToCluster[color1];
					delete colorToCluster[color2];
					colorToCluster[cluster1[0]] = cluster1;
					colorToCluster[cluster1[cluster1.length - 1]] = cluster1;
					lastCluster = cluster1;
			}

			// By now all colors should be in one cluster
			return lastCluster;
	}


	var createModel = function (params) {

		var model = {
			layout: false,
			viewId: 'wesite-home',
			controller: 'website-home-controller',
			title: 'Find Colors',
			site: params.site,
			colorArray: params.colorArray,
			colorCount: params.colorTotal,
			leaderboardName: params.leaderboardName
		}

		return model;

	}
};

module.exports = function(website) {
	var controller = new WebsiteController(website);
	website.get('/colors/', controller.get);
	website.get('/colors/:loc', controller.get);
};
