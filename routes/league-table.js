var MongoClient = require('mongodb').MongoClient;
var moment = require('moment');

var WebsiteController = function (website) {
  var uri = website.settings.dbconnection;

  function parseDate(dateString){
    // console.log(moment(dateString).isValid())
    // if(!moment(dateString).isValid()) return dateString;

    return moment(dateString,'YYYY MM DD').toDate();
  }

  function getHighestColors(){
    return new Promise((resolve, reject) => {
      MongoClient.connect(uri, function(err, db) {
        var collection = 'colorfinder';
  			if(err){
  				console.error(err);
  				return;
  			}
        db.collection(collection).find().sort({colorTotal: -1}).toArray((err, results) => {
  					if(err){
  						console.log(err);
  						reject(err);
  					}

            db.close();
            resolve(results);
  			});
      });
    }).catch((error) => {
      console.log(error);
    });
  }

  // Public functions
	this.get = function(req, res) {

      getHighestColors()
      .then((results) => {
        // sort results

        var hashesFound = {};
        function hash(o){
            return o.site;
        }
        results.forEach(function(o){
            hashesFound[hash(o)] = o;
        });

        var output = Object.keys(hashesFound).map(function(k){
            return hashesFound[k];
        });

        res.render('league-table', createModel({results: output}));

      });

	};

	var createModel = function (params) {

		var model = {
      layout: false,
			viewId: 'wesite-home',
			controller: 'website-home-controller',
			title: 'Find Colors',
			results: params.results
		}

		return model;

	}
};

module.exports = function(website) {
	var leagueTable = new WebsiteController(website);
	website.get(['/leaderboard/all', '/leaderboard/', '/leaderboard'], leagueTable.get);
};
