(function(window, $, undefined){
	"use strict";

	var Website = Website || {};

	Website = function(){

		

		function init(){
			
			
		}

		init();
	};

	var website = new Website();

})(window, jQuery);
(function(window, $, undefined){
	"use strict";

	var ColorFilter = ColorFilter || {};

	ColorFilter = function($container){

		var _self = this;
		var file = null;
		var event = new Event('build');
		var win = window;
		var colArr = [];
		var site = 'default';

		function init(){
			colArr = [];
			// Listen for the event.
			win.addEventListener('build', function (e) {
				getColors(e.response);
			}, false);

			site = $('[data-color-list]').data('color-list');

			//temp file - to be passed in
			var files = ['/uploads/' + site + '.css'];

			getCSSFiles(files);

		}

		function getColors(str){
	    // find all strings staring with # and ending in ;
	    var reHex = /#\w+;/g,
	    reRgb = /rgb\w+;/g,
	    item;

			while (item = reHex.exec(str)){
				var color = item[0].replace(';','');
				if(colArr.indexOf(color) === -1){
	    		colArr.push(color);
	    	}
	  	}

	  	while (item = reRgb.exec(str)){
	  		var color = item[0].replace(';','');
				if(colArr.indexOf(color) === -1){
	    		colArr.push(color);
	    	}
	  	}

	  	var rgbArr = colArr.map(hexToRgb);
			var sortedRgbArr = sortColors(rgbArr);
			var uniqueArr = removeDuplicates(sortedRgbArr);
			var finalArray = uniqueArr.map(rgbToHex);


	  	buildVisual(finalArray);

		}

		function removeDuplicates(list){
			var result = [];
	    $.each(list, function(i, e) {
	        if ($.inArray(e, result) == -1) result.push(e);
	    });
	    return result;
		}

		function buildVisual(colors){
			var colorList = [];
			for (var i = 0; i < colors.length; i++){
				var $color = $('<div />',
				{
					class:'color-item',
					html: '<span class="color-item__color" style="background-color: ' + colors[i] +' "></span><span class="color-item__title">' + colors[i] + '</span>'
				});
				colorList.push($color);
				$container.append($color);


			}

			$('[data-color-count]').text(colors.length);

		}

		function hexToRgb(hex) {
		     // Expand shorthand form (e.g. "03F") to full form (e.g. "0033FF")
	    var shorthandRegex = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
	    hex = hex.replace(shorthandRegex, function(m, r, g, b) {
	        return r + r + g + g + b + b;
	    });

	    var result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);
	    return result ? [
	        parseInt(result[1], 16), //r
	        parseInt(result[2], 16), //g
	        parseInt(result[3], 16) //b
	    ] : null;
		}

		function rgbToHex(rgb) {
		    return "#" + ((1 << 24) + (rgb[0] << 16) + (rgb[1] << 8) + rgb[2]).toString(16).slice(1);
		}

		function colorDistance(color1, color2) {
		    // This is actually the square of the distance but
		    // this doesn't matter for sorting.
		    var result = 0;
		    for (var i = 0; i < color1.length; i++)
		        result += (color1[i] - color2[i]) * (color1[i] - color2[i]);
		    return result;
		}

		function sortColors(colors) {
		    // Calculate distance between each color
		    var distances = [];
		    for (var i = 0; i < colors.length; i++) {
		        distances[i] = [];
		        for (var j = 0; j < i; j++)
		            distances.push([colors[i], colors[j], colorDistance(colors[i], colors[j])]);
		    }
		    distances.sort(function(a, b) {
		        return a[2] - b[2];
		    });

		    // Put each color into separate cluster initially
		    var colorToCluster = {};
		    for (var i = 0; i < colors.length; i++)
		        colorToCluster[colors[i]] = [colors[i]];

		    // Merge clusters, starting with lowest distances
		    var lastCluster;
		    for (var i = 0; i < distances.length; i++) {
		        var color1 = distances[i][0];
		        var color2 = distances[i][1];
		        var cluster1 = colorToCluster[color1];
		        var cluster2 = colorToCluster[color2];
		        if (!cluster1 || !cluster2 || cluster1 == cluster2)
		            continue;

		        // Make sure color1 is at the end of its cluster and
		        // color2 at the beginning.
		        if (color1 != cluster1[cluster1.length - 1])
		            cluster1.reverse();
		        if (color2 != cluster2[0])
		            cluster2.reverse();

		        // Merge cluster2 into cluster1
		        cluster1.push.apply(cluster1, cluster2);
		        delete colorToCluster[color1];
		        delete colorToCluster[color2];
		        colorToCluster[cluster1[0]] = cluster1;
		        colorToCluster[cluster1[cluster1.length - 1]] = cluster1;
		        lastCluster = cluster1;
		    }

		    // By now all colors should be in one cluster
		    return lastCluster;
		}

		function getCSSFiles(files){
			var $css = $('<div />');
			for(var i = 0; i < files.length; i++){
				$css.load(files[i], function(response, status, xhr){
					if(status === 'error'){
						alert('could not load ' + files[i]);
					}
					event.response = response;
	  			win.dispatchEvent(event);
				});
			}
		}

		init();
	};

	$().ready(function(){

		// var $container = $('[data-color-list]');
		// $container.each(function(i, e){
		// 	var colorFilter = new ColorFilter($(e));
		// });

	});


})(window, jQuery);
