'use strict';

var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../website');

chai.use(chaiHttp);

describe('As a test test', () => {

    var expect = chai.expect;

    describe('When I request the homepage', () => {

        it(' should return a status of 200', (done) => {
          chai.request(server)
          .get('/')
          .end((err, res) => {
            expect(res).to.have.status(200);
            done();
          });


        });
    });

    // describe('when I search for a url', () => {
    //     it('should return a')
    // });

});
